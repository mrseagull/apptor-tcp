var exec = require('cordova/exec');

exports.ping = function (arg0, arg1, arg2, success, error) {
    exec(success, error, 'PingServer', 'ping', [arg0, arg1, arg2]);
};

