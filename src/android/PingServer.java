package ru.apptor.ping;

import android.widget.Toast;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
/**
 * This class echoes a string called from JavaScript.
 */
public class PingServer extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("ping")) {
            if (args != null && args.length() >= 3) {
                String ip = args.getString(0);
                String port = args.getString(1);
                String msg = args.getString(2);
                connect(msg, ip, Integer.valueOf(port), callbackContext);
                return true;
            }
        }
        return false;
    }



    public static void connect(final String msg, final String ip, int port, CallbackContext callbackContext) {
        boolean isError = true;
        Socket s = null;
        String resultResponse = null;
        String answer = null;
        try {
            InetAddress serverAddress = InetAddress.getByName(ip);
            s = new Socket();
            s.connect(new InetSocketAddress(serverAddress, port), 5000);
            s.setSoTimeout(20000);
            s.setSoLinger(true,1000);

            BufferedReader in = null;
            PrintWriter out = null;
            if (s.isConnected()) {
                try {
                    in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
                    out.println(msg);
                    out.flush();
                    resultResponse = in.readLine();
                    isError = false;
                } catch (IOException ioe) {
                    resultResponse = ioe.toString();
                } finally {
                    if (s != null) {
                        try {
                            out.close();
                            in.close();
                            s.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            resultResponse = e.toString();
                        }
                    }
                }
            }else{
                resultResponse = "No connection";
            }
        }
        catch (Exception e)
        {
            resultResponse = e.toString();
        }
        if (isError) {
            callbackContext.error(resultResponse);
        } else {
            callbackContext.success(resultResponse);
        }

    }
}
