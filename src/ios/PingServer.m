/********* PingServer.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "PingServer.h"
#import "RNApptorSimpleTcp.h"

RNApptorSimpleTcp *commutator;
@implementation PingServer
- (void)ping:(CDVInvokedUrlCommand*)command
{
    
    NSString* ip = [command.arguments objectAtIndex:0];
    NSString* port = [command.arguments objectAtIndex:1];
    NSString* msg = [command.arguments objectAtIndex:2];
    commutator = [RNApptorSimpleTcp new];
    [commutator sendCommand:msg toServerWithName:ip port:port timeout:5000 complitionBlock:^(NSString *result) {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } erroringBlock:^(NSString *result) {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:result];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    /*
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }*/

    
}

@end

